FROM ubuntu:20.04

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install openjdk-11-jre wget unzip

RUN wget https://connect2id.com/assets/products/server/download/10.0-rc.2/Connect2id-server.zip && \
	unzip Connect2id-server.zip && \
	rm Connect2id-server.zip

ENV CATALINA_OPTS=""

		
EXPOSE 8080

ENTRYPOINT ["/connect2id-server-10.0-20200710/tomcat/bin/catalina.sh", "run"]