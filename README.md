### Connect2id server as a Docker image to be run as a Docker service inside a swarm cluster.

### This component is for testing purposes only!


### Prerequisites
- Docker swarm cluster run: ```docker swarm init```
- Public overlay network named 'public' run: ```docker network create -d overlay public```

### Run the project
- Be sure you are in the base path of the project. Open a terminal.
- To build and tag the Docker image run: ```docker build -t connect2idserver:10 .```
- After that run ```docker stack deploy c2id --compose-file docker-compose.yml```